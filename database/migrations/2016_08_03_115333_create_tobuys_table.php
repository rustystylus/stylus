<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTobuysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tobuys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cost')->unsigned()->default(0);
            $table->integer('priority')->unsigned()->default(3);
            $table->string('description', 255);
            $table->date('completed_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tobuys');
    }
}

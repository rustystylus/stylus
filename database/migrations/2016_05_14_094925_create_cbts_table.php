<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCbtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cbts', function (Blueprint $table) {
            $table->increments('id');
            $table->text('trigger');
            $table->text('thought');
            $table->text('alternative');
            $table->text('behaviour');
            $table->text('outcome');
            $table->text('next_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cbts');
    }
}

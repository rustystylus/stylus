<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tobuy extends Model
{
    protected $fillable = [
        'description',
        'cost',
        'priority',

    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Freq extends Model
{

	protected $fillable = [
        'type',
    ];
    
    public function todos()
    {
        return $this->hasMany('App\Todo','freq_id');
    }
}

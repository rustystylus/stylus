<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cbt extends Model
{
   protected $fillable = [
                'trigger',
                'thought',
                'alternative',
                'behaviour' ,
                'outcome',
                'next_time',
    ];
}

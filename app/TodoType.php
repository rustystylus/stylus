<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TodoType extends Model
{

	protected $fillable = [
        'type',
    ];
    public function todos()
    {
        return $this->hasMany('App\Todo','todotype_id');
    }
}

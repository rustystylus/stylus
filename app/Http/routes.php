<?php
Route::get('/', 'HomeController@index');
Route::get('/index', 'HomeController@index');

Route::get('/back', 'HomeController@back');

Route::resource('todos', 'TodosController');
	Route::get('todos/{id}/priorityUp', array('as' => 'priorityUp', 'uses' => 'TodosController@priorityUp'));
	Route::get('todos/{id}/priorityDown', array('as' => 'priorityDown', 'uses' => 'TodosController@priorityDown'));
	Route::get('todos/{id}/done', array('as' => 'done', 'uses' => 'TodosController@done'));
	Route::get('todos/{id}/destroy', array('as' => 'destroy', 'uses' => 'TodosController@destroy'));
Route::resource('tobuys', 'TobuysController');
	Route::get('tobuys/{id}/priorityUp', array('as' => 'priorityUp', 'uses' => 'TobuysController@priorityUp'));
	Route::get('tobuys/{id}/priorityDown', array('as' => 'priorityDown', 'uses' => 'TobuysController@priorityDown'));
	Route::get('tobuys/{id}/done', array('as' => 'done', 'uses' => 'TobuysController@done'));
	Route::get('tobuys/{id}/destroy', array('as' => 'destroy', 'uses' => 'TobuysController@destroy'));

Route::resource('questions', 'QuestionsController');
Route::resource('statements', 'StatementsController');
Route::resource('cbts', 'CbtsController');
Route::resource('todotypes', 'TodoTypesController');
Route::resource('freqs', 'FreqsController');

Route::resource('frontCbts', 'HomeController@frontCbts');
Route::resource('frontQuestions', 'HomeController@frontQuestions');
Route::resource('frontStatements', 'HomeController@frontStatements');
Route::resource('frontTodos', 'HomeController@frontTodos');
Route::resource('frontTobuys', 'HomeController@frontTobuys');

Route::auth();

Route::get('/home', 'HomeController@index');

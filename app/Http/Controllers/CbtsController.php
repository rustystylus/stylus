<?php

namespace App\Http\Controllers;

use DB;
use App\Cbt;

use Illuminate\Http\Request;

use App\Http\Requests;

class CbtsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
    	$cbts = Cbt::all();

    	return view('cbts.index', compact('cbts'))
    			->with('title', 'cbts');
    }

    public function create()
    {
        return view('cbts.create')
                ->with('title','cbts');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
                'trigger' => 'required',
                'thought' => 'required',
                'alternative' => 'required',
                'behaviour' => 'required',
                'outcome'=> 'required',
                'next_time' => 'required',
        ]);
        $input = $request->all();

        Cbt::create($input);

        \Session::flash('flash_message', 'cbt successfully added!');

        return redirect()->back();
    }

    public function show( $id)
    {
        $cbt = Cbt::findOrFail($id);
        return view('cbts.show')->withcbt($cbt);
    }

    public function  edit($id)
    {
        $cbt = Cbt::findOrFail($id);
        return view('cbts.edit')->withcbt($cbt);
    }
    public function  update($id, Request $request)
    {
        $cbt = Cbt::findOrFail($id);
        $this->validate($request, [
                'trigger' => 'required',
                'thought' => 'required',
                'alternative' => 'required',
                'behaviour' => 'required',
                'outcome'=> 'required',
                'next_time' => 'required',
        ]);

        $input = $request->all();
        $cbt->fill($input)->save();

        \Session::flash('flash_message', 'cbt successfully added!');

        return redirect()->back();
    }
    public function destroy(cbt $cbt)
    {

    }
}

<?php

namespace App\Http\Controllers;

use DB;
use App\Statement;

use Illuminate\Http\Request;

use App\Http\Requests;

class StatementsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$statements = Statement::all();

    	return view('statements.index', compact('statements'))
    			->with('title', 'statements');
    }

    public function create()
    {
        return view('statements.create')
                ->with('title','statements');
    }
    public function store(Request $request)
    {
      //  dd($request->all());
        $this->validate($request, [
                'statement' => 'required',
        ]);
        $input = $request->all();

        Statement::create($input);

        \Session::flash('flash_message', 'statement successfully added!');

        return redirect()->back();
    }
    public function show( $id)
    {
        $statement = statement::findOrFail($id);
        return view('statements.show')->withstatement($statement);
    }

    public function  edit($id)
    {
        $statement = statement::findOrFail($id);
        return view('statements.edit')->withstatement($statement);
    }
    public function  update($id, Request $request)
    {
        $statement = statement::findOrFail($id);
        $this->validate($request, [
                 'statement' => 'required',
        ]);

        $input = $request->all();
        $statement->fill($input)->save();

        \Session::flash('flash_message', 'statement successfully added!');

        return redirect()->back();
    }
    public function destroy($id)
    {
        $statement = statement::findOrFail($id);
        $statement->delete();
        \Session::flash('flash_message', 'statement successfully deleted!');
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use DB;
use App\Question;

use Illuminate\Http\Request;

use App\Http\Requests;

class QuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    } 
    
    public function index()
    {
    	$questions = Question::all();

    	return view('questions.index', compact('questions'))
    			->with('title', 'questions');
    }

    public function create()
    {
        return view('questions.create')
                ->with('title','questions');
    }

    public function store(Request $request)
    {
      //  dd($request->all());
        $this->validate($request, [
                'question' => 'required',
                'answer' => 'required',
        ]);
        $input = $request->all();

        Question::create($input);

        \Session::flash('flash_message', 'question successfully added!');

        return redirect()->back();
    }
    
    public function show( $id)
    {
        $question = Question::findOrFail($id);
        return view('questions.show')->withquestion($question);
    }

    public function  edit($id)
    {
        $question = Question::findOrFail($id);
        return view('questions.edit')->withquestion($question);
    }
    public function  update($id, Request $request)
    {
        $question = Question::findOrFail($id);
        $this->validate($request, [
                'question' => 'required',
                'answer' => 'required',
        ]);

        $input = $request->all();
        $question->fill($input)->save();

        \Session::flash('flash_message', 'question successfully added!');

        return redirect()->back();
    }
    public function destroy(question $question)
    {

    }
}

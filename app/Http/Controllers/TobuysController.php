<?php

namespace App\Http\Controllers;

use DB;
use App\Tobuy;

use Illuminate\Http\Request;

use App\Http\Requests;

class TobuysController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    } 
    
    public function index()
    {
    	$tobuys = Tobuy::orderBy('priority','desc')
                    ->get();
        
    	return view('tobuys.index', compact('tobuys'))
    			->with('title', 'tobuys');
    }

    public function create()
    {
        return view('tobuys.create')
                ->with('title','tobuys');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
                'description' => 'required',
                'priority' => 'required',
                'cost'=> 'required'
        ]);
        $input = $request->all();

        Tobuy::create($input);

        \Session::flash('flash_message', 'tobuy successfully added!');

        return redirect()->back();
    }
    public function show( $id)
    {
        $tobuy = Tobuy::findOrFail($id);
        return view('tobuys.show')->withtobuy($tobuy);
    }

    public function  edit($id)
    {
        $tobuy = Tobuy::findOrFail($id);
        return view('tobuys.edit')->withtobuy($tobuy);
    }
    public function  update($id, Request $request)
    {
        $tobuy = Tobuy::findOrFail($id);
        $this->validate($request, [
                'description' => 'required',
                'priority' => 'required',
                'cost'=> 'required'
        ]);

        $input = $request->all();
        $tobuy->fill($input)->save();

        \Session::flash('flash_message', 'tobuy successfully added!');

        return redirect()->back();
    }
    public function destroy($id)
    {
        $tobuy = Tobuy::findOrFail($id);
        $tobuy->delete();
        \Session::flash('flash_message', 'tobuy successfully deleted!');
        return redirect()->back();
    }
    public function priorityUp($id)
    {
        $tobuy = Tobuy::findOrFail($id);
        if($tobuy)
        {   $tobuy->priority ++;
            $tobuy->save();
        }
        return redirect()->back();
    }

    public function priorityDown($id)
    {
        $tobuy = Tobuy::findOrFail($id);
        if($tobuy)
        {   if($tobuy->priority > 0)
            $tobuy->priority --;
            $tobuy->save();
        }
        return redirect()->back();
    }

    public function done($id)
    {
        $tobuy = Tobuy::findOrFail($id);
        if($tobuy->completed_at === null)
        {   $tobuy->priority=0;
            $tobuy->completed_at = date("Y-m-d",strtotime("now"));
            $tobuy->save();
        }
        else
        {   
            $tobuy->priority=1;
            $tobuy->completed_at = null;
            $tobuy->save();
        }    
        return redirect()->back();
    }
}

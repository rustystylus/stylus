<?php

namespace App\Http\Controllers;

use DB;
use App\TodoType;
use Illuminate\Http\Request;

use App\Http\Requests;

class TodoTypesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$todotypes = TodoType::all();

    	return view('todotypes.index', compact('todotypes'))
    			->with('title', 'todotypes');
    }

    public function create()
    {
        return view('todotypes.create')
                ->with('title','todotypes');
    }
    public function store(Request $request)
    {
      //  dd($request->all());
        $this->validate($request, [
                'type' => 'required',
        ]);
        $input = $request->all();

        TodoType::create($input);

        \Session::flash('flash_message', 'todotype successfully added!');

        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use DB;
use App\Freq;
use Illuminate\Http\Request;

use App\Http\Requests;

class FreqsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$freqs = Freq::all();

    	return view('freqs.index', compact('freqs'))
    			->with('title', 'freqs');
    }

    public function create()
    {
        return view('freqs.create')
                ->with('title','freqs');
    }
    public function store(Request $request)
    {
      //  dd($request->all());
        $this->validate($request, [
                'type' => 'required',
        ]);
        $input = $request->all();

        Freq::create($input);

        \Session::flash('flash_message', 'freq successfully added!');

        return redirect()->back();
    }
}

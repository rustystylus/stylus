<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index','frontTodos','frontTobuys','frontStatements',
                          'frontQuestions', 'frontCbts']]);
    }

    public function index()
    {
        return view('index')
        ->with('title','index')
        ->with('content','');
    }

    public function back()
    {
        return view('dashboard')
        ->with('title', 'Dashboard')
        ->with('content', 'dashboard content');
    }

    public function frontTodos()
    {
       // get all the photos
      $todos = \App\Todo::where('completed_at',null)
                    ->orderBy('todotype_id', 'asc')
                    ->orderBy('priority','desc')
                    ->get();
                    //->paginate(15);  
      $types = \App\TodoType::all();  
      $freqs = \App\Freq::all();
      $dones = \App\Todo::where('completed_at', '<>', '')
              ->orderBy('todotype_id', 'asc')
              ->orderBy('priority','desc')
              ->paginate(15);                        
    // dd($types);die();
       // load the view and pass the gallery pics
       return view('todos.front') 
                    ->with('todos', $todos)
                    ->with('types', $types)
                    ->with('freqs', $freqs)
                    ->with('dones', $dones);
    }
    
    public function frontTobuys()
    {
       // get all the photos
      $tobuys = \App\Tobuy::where('completed_at',null)
                    ->orderBy('priority','desc')
                    ->get();
                    //->paginate(15);  

      $dones = \App\Tobuy::where('completed_at', '<>', '')
              ->orderBy('priority','desc')
              ->paginate(15);                        
    // dd($types);die();
       // load the view and pass the gallery pics
       return view('tobuys.front') 
                    ->with('tobuys', $tobuys)
                    ->with('dones', $dones);
    }

    public function frontStatements()
    {
       $statements = \App\Statement::all();
        //dd($statements);die();         
       return view('statements.front') 
                     ->with('statements', $statements);
    }

    public function frontQuestions()
    {
       $questions = \App\Question::all();
        //dd($statements);die();         
       return view('questions.front') 
                     ->with('questions', $questions);
    } 

    public function frontCbts()
    {
       $cbts = \App\Cbt::all();
         
       return view('cbts.front') 
                     ->with('cbts', $cbts);
    }       
}

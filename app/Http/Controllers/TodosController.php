<?php

namespace App\Http\Controllers;

use DB;
use App\Todo;

use Illuminate\Http\Request;

use App\Http\Requests;

class TodosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    } 
    
    public function index()
    {
    	$todos = Todo::orderBy('todotype_id', 'asc')
                    ->orderBy('priority','desc')
                    ->get();
        
    	return view('todos.index', compact('todos'))
    			->with('title', 'todos');
    }

    public function create()
    {
        return view('todos.create')
                ->with('title','todos');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
                'description' => 'required',
                'priority' => 'required',
                'todotype_id'=> 'required'
        ]);
        $input = $request->all();

        Todo::create($input);

        \Session::flash('flash_message', 'Todo successfully added!');

        return redirect()->back();
    }
    public function show( $id)
    {
        $todo = Todo::findOrFail($id);
        return view('todos.show')->withtodo($todo);
    }

    public function  edit($id)
    {
        $todo = Todo::findOrFail($id);
        return view('todos.edit')->withtodo($todo);
    }
    public function  update($id, Request $request)
    {
        $todo = Todo::findOrFail($id);
        $this->validate($request, [
                'description' => 'required',
                'priority' => 'required',
                'todotype_id'=> 'required'
        ]);

        $input = $request->all();
        $todo->fill($input)->save();

        \Session::flash('flash_message', 'todo successfully added!');
        return redirect()->action('HomeController@frontTodos@index');
       // return redirect()->back();
    }
    public function destroy($id)
    {
        $todo = Todo::findOrFail($id);
        $todo->delete();
        \Session::flash('flash_message', 'todo successfully deleted!');
        return redirect()->back();
    }
    public function priorityUp($id)
    {
        $todo = Todo::findOrFail($id);
        if($todo)
        {   $todo->priority ++;
            $todo->save();
        }
        return redirect()->back();
    }

    public function priorityDown($id)
    {
        $todo = Todo::findOrFail($id);
        if($todo)
        {   if($todo->priority > 0)
            $todo->priority --;
            $todo->save();
        }
        return redirect()->back();
    }

    public function done($id)
    {
        $todo = Todo::findOrFail($id);
        if($todo->completed_at === null)
        {   $todo->priority=0;
            $todo->completed_at = date("Y-m-d",strtotime("now"));
            $todo->save();
        }
        else
        {   
            $todo->priority=1;
            $todo->completed_at = null;
            $todo->save();
        }    
        return redirect()->back();
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    protected $fillable = [
        'description',
        'priority',
        'todotype_id',
        'freq_id'
    ];

    public function todotypes()
	{
	    return $this->belongsTo('App\TodoType');
	}

    public function freqs()
	{
	    return $this->belongsTo('App\Freq');
	}
}

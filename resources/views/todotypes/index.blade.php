@extends('layouts.back')

@section('title')
@stop
@section('content')

	@if(Session::has('flash_message'))
	    <div class="alert alert-success">
	        {{ Session::get('flash_message') }}
	    </div>
	@endif
<div class="col-md-1">

</div>
<div class="col-md-10">
<h1>todotypes</h1>
	<a href="{{ route('todotypes.create') }}" class="btn btn-primary">Add New todotype</a>
	<table class="table table-striped table-bordered">
		<thead>
		    <tr>
		        <td>todotype</td>
		    </tr>
		</thead>
	@foreach($todotypes as $todotype)
	    <tr>
		    <td>{{ $todotype->type }}</td>
    
		    <td>
		        <a href="{{ route('todotypes.show', $todotype->id) }}" class="btn btn-info">View todotype</a>
		        <a href="{{ route('todotypes.edit', $todotype->id) }}" class="btn btn-primary">Edit todotype</a>
		    </td>
	    </tr>
	@endforeach
	</table>
</div>
<div class="col-md-1">

</div>
@stop
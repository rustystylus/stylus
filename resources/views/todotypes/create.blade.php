@extends('layouts.back')

@section('title')
@stop
@section('content')
	<h1>todotypes</h1>

		@if(Session::has('flash_message'))
		    <div class="alert alert-success">
		        {{ Session::get('flash_message') }}
		    </div>
		@endif

		@include('partials.alerts.errors')

	{!! Form::open([
	    'route' => 'todotypes.store'
	]) !!}

	<div class="form-group">
	    {!! Form::label('type', 'type', ['class' => 'control-label']) !!}
	    {!! Form::text('type', null, ['class' => 'form-control']) !!}
	</div>

	{!! Form::submit('Create New todotype', ['class' => 'btn btn-primary']) !!}

	{!! Form::close() !!}
@stop
@extends('layouts.back')

@section('title')
cbts
@stop
@section('content')
	<b>cbt: {{$cbt->trigger}}</b>
	<p>{{$cbt->thought}}</p>
	<p>{{$cbt->alternative}}</p>
	<p>{{$cbt->behaviour}}</p>
	<p>{{$cbt->outcome}}</p>
	<p>{{$cbt->next_time}}</p>
@stop
@extends('layouts.back')

@section('title')
@stop
@section('content')

	@if(Session::has('flash_message'))
	    <div class="alert alert-success">
	        {{ Session::get('flash_message') }}
	    </div>
	@endif
<div class="col-md-2">
<h1>cbts</h1>
</div>
<div class="col-md-8">

	<a href="{{ route('cbts.create') }}" class="btn btn-primary">Add New cbt</a>
	<table class="table table-striped table-bordered">
		<thead>
		    <tr>
		        <td>Trigger</td>
		        <td>Thoughts</td>
		        <td>Alternative Thoughts</td>
		        <td>Behaviour</td>
		        <td>Outcome</td>
		        <td>Next Time</td>
		    </tr>
		</thead>
	@foreach($cbts as $cbt)
	    <tr>
		    <td>{{ $cbt->trigger }}</td>
		    <td>{{ $cbt->thought}}</td> 
		     <td>{{ $cbt->alternative}}</td> 
		     <td>{{ $cbt->behaviour}}</td>
		     <td>{{ $cbt->outcome}}</td> 
		     <td>{{ $cbt->next_time}}</td>  
		    <td>
		        <a href="{{ route('cbts.show', $cbt->id) }}" class="btn btn-info">View cbt</a>
		        <a href="{{ route('cbts.edit', $cbt->id) }}" class="btn btn-primary">Edit cbt</a>
		    </td>
	    </tr>
	@endforeach
	</table>
</div>
<div class="col-md-2">

</div>
@stop
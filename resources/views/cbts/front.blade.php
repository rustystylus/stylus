@extends('layouts.front')

@section('title')
cbts
@stop
@section('content')

<script>
  $(function() {
    $( "#accordion" ).accordion({
      collapsible: true,
      active: false
    });
  });
</script>

<div class="col-md-2">

</div>
<div class="col-md-8">

		<h1>cbts</h1>
			<div id="accordion">

				@foreach($cbts as $cbt)

				    <h3> {{ $cbt->trigger }}</h3>
				    <div>
					    <p><em>thought: </em>{{ $cbt->thought}}</p>
					    <p><em>alternative: </em> {{ $cbt->alternative}}</p>
					    <p><em>behaviour: </em> {{ $cbt->behaviour}}</p>
					    <p><em>outcome: </em> {{ $cbt->outcome}}</p>
					    <p><em>next time:  </em>{{ $cbt->next_time}}</p>
		    		</div>

				@endforeach
			</div>

</div>
<div class="col-md-2">

</div>
@stop
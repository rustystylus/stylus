@extends('layouts.back')
@section('title')
cbts
@stop
@section('content')

	@if(Session::has('flash_message'))
	    <div class="alert alert-success">
	        {{ Session::get('flash_message') }}
	    </div>
	@endif
	
	@include('partials.alerts.errors')

<h1>Edit cbt </h1>
{!! Form::model($cbt, [
    'method' => 'PATCH',
    'route' => ['cbts.update', $cbt->id]
]) !!}

<div class="form-group">
    {!! Form::label('trigger', 'trigger:', ['class' => 'control-label']) !!}
    {!! Form::text('trigger', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('thought', 'thought:', ['class' => 'control-label']) !!}
    {!! Form::text('thought', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('alternative', 'alternative thoughts:', ['class' => 'control-label']) !!}
    {!! Form::text('alternative', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('behaviour', 'behaviour :', ['class' => 'control-label']) !!}
    {!! Form::text('behaviour', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('outcome', 'outcome:', ['class' => 'control-label']) !!}
    {!! Form::text('outcome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('next_time', 'next_time :', ['class' => 'control-label']) !!}
    {!! Form::text('next_time', null, ['class' => 'form-control']) !!}
</div>
{!! Form::submit('Update cbt', ['class' => 'btn btn-primary']) !!}

{!! Form::close() !!}
 <a href="{{ route('cbts.index') }}">Go back to all cbts.</a>


@stop
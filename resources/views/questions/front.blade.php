@extends('layouts.front')

@section('title')
questions
@stop
@section('content')

<script>
  $(function() {
    $( "#accordion" ).accordion({
      collapsible: true,
      active: false
    });
  });
</script>

<div class="col-md-2">

</div>
<div class="col-md-8">

		<h1>Questions</h1>
			<div id="accordion">

				@foreach($questions as $question)

				    <h3> {{ $question->question }}</h3>
				    <div>
				    <p>{{ $question->answer}}</p>
		    		</div>

				@endforeach
			</div>

</div>
<div class="col-md-2">

</div>
@stop
@extends('layouts.back')
@section('title')
questions
@stop
@section('content')

	@if(Session::has('flash_message'))
	    <div class="alert alert-success">
	        {{ Session::get('flash_message') }}
	    </div>
	@endif
	
	@include('partials.alerts.errors')

<h1>Edit question </h1>
{!! Form::model($question, [
    'method' => 'PATCH',
    'route' => ['questions.update', $question->id]
]) !!}

<div class="form-group">
    {!! Form::label('question', 'question:', ['class' => 'control-label']) !!}
    {!! Form::text('question', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('answer', 'answer:', ['class' => 'control-label']) !!}
    {!! Form::text('answer', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit('Update question', ['class' => 'btn btn-primary']) !!}

{!! Form::close() !!}
 <a href="{{ route('questions.index') }}">Go back to all questions.</a>


@stop
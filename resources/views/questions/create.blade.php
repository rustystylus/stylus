@extends('layouts.back')

@section('title')
@stop
@section('content')
<h1>Create a new question</h1>

	@if(Session::has('flash_message'))
	    <div class="alert alert-success">
	        {{ Session::get('flash_message') }}
	    </div>
	@endif

	@include('partials.alerts.errors')

{!! Form::open([
    'route' => 'questions.store'
]) !!}

<div class="form-group">
    {!! Form::label('question', 'question:', ['class' => 'control-label']) !!}
    {!! Form::text('question', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('answer', 'answer:', ['class' => 'control-label']) !!}
    {!! Form::text('answer', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit('Create New question', ['class' => 'btn btn-primary']) !!}

{!! Form::close() !!}
@stop
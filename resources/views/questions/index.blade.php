@extends('layouts.back')

@section('title')
@stop
@section('content')

	@if(Session::has('flash_message'))
	    <div class="alert alert-success">
	        {{ Session::get('flash_message') }}
	    </div>
	@endif
<div class="col-md-2">
<h1>questions</h1>
</div>
<div class="col-md-8">

	<a href="{{ route('questions.create') }}" class="btn btn-primary">Add New question</a>
	<table class="table table-striped table-bordered">
		<thead>
		    <tr>
		        <td>Question</td>
		        <td>Answer</td>
		    </tr>
		</thead>
	@foreach($questions as $question)
	    <tr>
		    <td>{{ $question->question }}</td>
		    <td>{{ $question->answer}}</td>    
		    <td>
		        <a href="{{ route('questions.show', $question->id) }}" class="btn btn-info">View question</a>
		        <a href="{{ route('questions.edit', $question->id) }}" class="btn btn-primary">Edit question</a>
		    </td>
	    </tr>
	@endforeach
	</table>
</div>
<div class="col-md-2">

</div>
@stop
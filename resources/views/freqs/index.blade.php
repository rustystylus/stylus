@extends('layouts.back')

@section('title')
@stop
@section('content')

	@if(Session::has('flash_message'))
	    <div class="alert alert-success">
	        {{ Session::get('flash_message') }}
	    </div>
	@endif
<div class="col-md-1">

</div>
<div class="col-md-10">
<h1>freqs</h1>
	<a href="{{ route('freqs.create') }}" class="btn btn-primary">Add New freq</a>
	<table class="table table-striped table-bordered">
		<thead>
		    <tr>
		        <td>todotype</td>
		    </tr>
		</thead>
	@foreach($freqs as $todotype)
	    <tr>
		    <td>{{ $todotype->type }}</td>
    
		    <td>
		        <a href="{{ route('freqs.show', $todotype->id) }}" class="btn btn-info">View freq</a>
		        <a href="{{ route('freqs.edit', $todotype->id) }}" class="btn btn-primary">Edit freq</a>
		    </td>
	    </tr>
	@endforeach
	</table>
</div>
<div class="col-md-1">

</div>
@stop
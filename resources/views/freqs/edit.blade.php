@extends('layouts.back')
@section('title')
freqs
@stop
@section('content')

	@if(Session::has('flash_message'))
	    <div class="alert alert-success">
	        {{ Session::get('flash_message') }}
	    </div>
	@endif
	
	@include('partials.alerts.errors')

<h1>Edit freq </h1>
{!! Form::model($freq, [
    'method' => 'PATCH',
    'route' => ['freqs.update', $freq->id]
]) !!}

<div class="form-group">
    {!! Form::label('freq', 'freq', ['class' => 'control-label']) !!}
    {!! Form::text('freq', null, ['class' => 'form-control']) !!}
</div>


{!! Form::submit('Update freq', ['class' => 'btn btn-primary']) !!}

{!! Form::close() !!}
 <a href="{{ route('freqs.index') }}">Go back to all freqs.</a>


@stop
@extends('layouts.front')

@section('title')
Statements
@stop
@section('content')
  <script>
  $(function() {
    $( "#dialog" ).dialog({
  width: 400
});
  });
  </script>

<div class="col-md-2">

</div>
<div class="col-md-8">
		<h1>Statements</h1>

				@foreach($statements as $statement)
				    <p>{{ $statement->statement}}</p>
				@endforeach
</div>
<div class="col-md-2">

</div>

<div id="dialog" title="Basic dialog">
  <p>{{ $statements->random()->statement}}</p>
</div>
@stop
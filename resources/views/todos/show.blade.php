@extends('layouts.back')

@section('title')
todos
@stop
@section('content')
<ul>
	<li>description {{$todo->description}}</li>
	<li>priority {{$todo->priority}}</li>

	<li>completed date {{$todo->completed_at}}</li>
</ul>
@stop
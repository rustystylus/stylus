@extends('layouts.front')

@section('title')
Todos
@stop
@section('extra_scripts_css')
<!-- DATATABLES  -->  
    <link rel="stylesheet" href='//cdn.datatables.net/plug-ins/9dcbecd42ad/integration/bootstrap/3/dataTables.bootstrap.css'>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
   <script src="//cdn.datatables.net/plug-ins/9dcbecd42ad/integration/bootstrap/3/dataTables.bootstrap.js"></script>
@stop
@section('content')

<div class="col-md-1">

</div>
<div class="col-md-10">
	<h1>Todos</h1>
		<a href="{{ route('todos.create') }}" class="btn btn-primary">Add New todo</a>
		<div id="todotable">
			<table width="100%" class="display" id="todos" cellspacing="0">
		        <thead>
		            <tr>            	
		                <th></th>
		                <th></th>
		                <th></th>
				        <th>Todo</th>
				        <th>Type</th>
				        <th>Freq</th>
		                <th></th>
		                <th></th>				                
		                <th>Done</th> 
		                <th>Delete</th>   
		            </tr>
		        </thead>
		        <tfoot>
		            <tr>            	
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		            </tr>
		        </tfoot>
		        <tbody>
					@foreach($todos as $todo)
						<tr data-id={{ $todo->id }} >
						    <td>
							<span class="badge alert-todo">
						        {{$todo->priority}}
							</span>
						    </td>
						    <td><a href="{{ URL::action('TodosController@priorityUp', [$todo->id] ) }}"><span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span></a></td>
						    <td><a href="{{ URL::action('TodosController@priorityDown', [$todo->id] ) }}"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a></td>
							<td>
				    			{!!$todo->description!!}
							</td>
							<td>
								{{$types[$todo->todotype_id]->type}}
							</td>	
							<td>
				    			{{$freqs[$todo->freq_id]->type}}
							</td>																	
							<td>
								<a href="{{ URL::action('TodosController@edit', [$todo->id] ) }}">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								</a>
							</td>
							<td>
					    		{{date("j M", strtotime($todo->updated_at))}}
							</td>
							<td>
					    		<a href="{{ URL::action('TodosController@done', [$todo->id] ) }}">
					    			@if(is_null($todo->completed_at))
					    				<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
					    			@else
					    				<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
					    			@endif	
					    		</a>
							</td>
							<td>
								<script>
			                          function ConfirmDelete()
			                          {
			                              var x = confirm("Are you sure you want to delete?");
			                              if (x)
			                                return true;
			                              else
			                                return false;
			                          }
			                    </script>
							    <a  onclick="return ConfirmDelete()" href="{{ URL::action('TodosController@destroy', [$todo->id] ) }}"><span style="color:red" class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			<?php //echo $todos->render(); ?>
	</div>


	<div id="donetable">
			<table width="100%" class="display" id="dones" cellspacing="0">
		        <thead>
		            <tr>            	
		                <th>Priority</th>
		                <th></th>
		                <th></th>
				        <th>Todo</th>
				        <th>Type</th>
				        <th></th>
		                <th>Edit</th>
		                <th>Updated</th>				                
		                <th>Done</th> 
		                <th>Delete</th>   
		            </tr>
		        </thead>
		        <tfoot>
		            <tr>            	
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		            </tr>
		        </tfoot>
		        <tbody>
					@foreach($dones as $done)
						<tr data-id={{ $done->id }} >
						    <td>
							<span class="badge alert-done">
						        {{$done->priority}}
							</span>
						    </td>
						    <td><a href="{{ URL::action('TodosController@priorityUp', [$done->id] ) }}"><span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span></a></td>
						    <td><a href="{{ URL::action('TodosController@priorityDown', [$done->id] ) }}"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a></td>
							<td>
				    			{!!$done->description!!}
							</td>
							<td>
								{{$types[$done->todotype_id]->type}}
							</td>	
							<td>
				    			{{$done->daily}}
							</td>																	
							<td>
								<a href="{{ URL::action('TodosController@edit', [$done->id] ) }}">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								</a>
							</td>
							<td>
					    		{{date("j M", strtotime($done->updated_at))}}
							</td>
							<td>
					    		<a href="{{ URL::action('TodosController@done', [$done->id] ) }}">
					    			@if(is_null($done->completed_at))
					    				<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
					    			@else
					    				<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
					    			@endif	
					    		</a>
							</td>
							<td>
								<script>
			                          function ConfirmDelete()
			                          {
			                              var x = confirm("Are you sure you want to delete?");
			                              if (x)
			                                return true;
			                              else
			                                return false;
			                          }
			                    </script>
							    <a  onclick="return ConfirmDelete()" href="{{ URL::action('TodosController@destroy', [$done->id] ) }}"><span style="color:red" class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			<?php echo $dones->render(); ?>
	</div>
</div>
<div class="col-md-1">

</div>


	<script>
	    $(document).ready(function() {
	        $('#todos').dataTable({
	        	"order": [[ 0, "desc" ]],
	        	"iDisplayLength": 25,
	        	"aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]]
	        });

	    } );
	</script>

@stop
@extends('layouts.back')

@section('title')
@stop
@section('content')
<h1>Create a new todo</h1>

	@if(Session::has('flash_message'))
	    <div class="alert alert-success">
	        {{ Session::get('flash_message') }}
	    </div>
	@endif

	@include('partials.alerts.errors')

{!! Form::open([
    'route' => 'todos.store'
]) !!}

<div class="form-group">
    {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('priority', 'Priority', ['class' => 'control-label']) !!}
    {!! Form::number('priority', 3, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('type', 'Type', ['class' => 'control-label']) !!}
    {!! Form::select('todotype_id',  
        \App\TodoType::lists('type', 'id'),
        ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('freq', 'Freq', ['class' => 'control-label']) !!}
    {!! Form::select('freq_id',  
        \App\Freq::lists('type', 'id'),
        ['class' => 'form-control']) !!}
</div>
{!! Form::submit('Create New Todo', ['class' => 'btn btn-primary']) !!}

{!! Form::close() !!}
@stop
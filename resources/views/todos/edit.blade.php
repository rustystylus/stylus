@extends('layouts.back')
@section('title')
todos
@stop
@section('content')

	@if(Session::has('flash_message'))
	    <div class="alert alert-success">
	        {{ Session::get('flash_message') }}
	    </div>
	@endif
	
	@include('partials.alerts.errors')

<h1>Edit todo </h1>
{!! Form::model($todo, [
    'method' => 'PATCH',
    'route' => ['todos.update', $todo->id]
]) !!}


<div class="form-group">
    {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('priority', 'Priority', ['class' => 'control-label']) !!}
    {!! Form::number('priority', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('type', 'Type', ['class' => 'control-label']) !!}
    {!! Form::select('todotype_id',  
        \App\TodoType::lists('type', 'id'),
        ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('freq', 'Freq', ['class' => 'control-label']) !!}
    {!! Form::select('freq_id',  
        \App\Freq::lists('type', 'id'),
        ['class' => 'form-control']) !!}
</div>
{!! Form::submit('Update todo', ['class' => 'btn btn-primary']) !!}

{!! Form::close() !!}
 <a href="{{ route('todos.index') }}">Go back to all todos.</a>


@stop
@extends('layouts.back')

@section('title')
@stop
@section('content')

	@if(Session::has('flash_message'))
	    <div class="alert alert-success">
	        {{ Session::get('flash_message') }}
	    </div>
	@endif
<div class="col-md-1">

</div>
<div class="col-md-10">
<h1>todos</h1>
	<a href="{{ route('todos.create') }}" class="btn btn-primary">Add New todo</a>
	<table class="table table-striped table-bordered">
		<thead>
		    <tr>
		        <td>Description</td>
		        <td>Priority</td>
				<td>Daily</td>
				<td>Type</td>
				<td>Completed date</td>
		    </tr>
		</thead>
	@foreach($todos as $todo)
	    <tr>
		    <td>{{ $todo->description }}</td>
		    <td>{{ $todo->priority }}</td>
		    <td>{{ $todo->freq_id }}</td>
		    <td>{{ $todo->todotype_id }}</td>
		    <td>{{ $todo->completed_at }}</td>
		    <td>
		        <a href="{{ route('todos.show', $todo->id) }}"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>
		        <a href="{{ route('todos.edit', $todo->id) }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
		    </td>
	    </tr>
	@endforeach
	</table>
</div>
<div class="col-md-1">

</div>
@stop
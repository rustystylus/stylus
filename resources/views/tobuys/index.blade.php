@extends('layouts.back')

@section('title')
@stop
@section('content')

	@if(Session::has('flash_message'))
	    <div class="alert alert-success">
	        {{ Session::get('flash_message') }}
	    </div>
	@endif
<div class="col-md-1">

</div>
<div class="col-md-10">
<h1>tobuys</h1>
	<a href="{{ route('tobuys.create') }}" class="btn btn-primary">Add New tobuy</a>
	<table class="table table-striped table-bordered">
		<thead>
		    <tr>
		        <td>Description</td>
		        <td>Priority</td>
				<td>Cost £</td>
				<td>Completed date</td>
		    </tr>
		</thead>
	@foreach($tobuys as $tobuy)
	    <tr>
		    <td>{{ $tobuy->description }}</td>
		    <td>{{ $tobuy->priority }}</td>
		    <td>{{ $tobuy->cost }}</td>
		    <td>{{ $tobuy->completed_at }}</td>
		    <td>
		        <a href="{{ route('tobuys.edit', $tobuy->id) }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
		    </td>
	    </tr>
	@endforeach
	</table>
</div>
<div class="col-md-1">

</div>
@stop
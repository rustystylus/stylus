@extends('layouts.back')

@section('title')
@stop
@section('content')
<h1>Create a new tobuy</h1>

	@if(Session::has('flash_message'))
	    <div class="alert alert-success">
	        {{ Session::get('flash_message') }}
	    </div>
	@endif

	@include('partials.alerts.errors')

{!! Form::open([
    'route' => 'tobuys.store'
]) !!}

<div class="form-group">
    {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('cost', 'Cost(in pounds)', ['class' => 'control-label']) !!}
    {!! Form::number('cost', 0, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('priority', 'Priority', ['class' => 'control-label']) !!}
    {!! Form::number('priority', 3, ['class' => 'form-control']) !!}
</div>


{!! Form::submit('Create New tobuy', ['class' => 'btn btn-primary']) !!}

{!! Form::close() !!}
@stop
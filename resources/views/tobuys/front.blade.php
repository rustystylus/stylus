@extends('layouts.front')

@section('title')
tobuys
@stop
@section('extra_scripts_css')
<!-- DATATABLES  -->  
    <link rel="stylesheet" href='//cdn.datatables.net/plug-ins/9dcbecd42ad/integration/bootstrap/3/dataTables.bootstrap.css'>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
   <script src="//cdn.datatables.net/plug-ins/9dcbecd42ad/integration/bootstrap/3/dataTables.bootstrap.js"></script>
@stop
@section('content')

<div class="col-md-1">

</div>
<div class="col-md-10">
	<h1>Tobuys</h1>
		<div id="tobuytable">
			<table width="100%" class="display" id="tobuys" cellspacing="0">
		        <thead>
		            <tr>            	
		                <th>Priority</th>
		                <th>Up</th>
		                <th>Down</th>
				        <th>Description</th>
				        <th>Cost &pound;</th>
		                <th>Edit</th>
		                <th>Updated</th>				                
		                <th>Bought</th> 
		                <th>Delete</th>   
		            </tr>
		        </thead>
		        <tfoot>
		            <tr>            	
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		            </tr>
		        </tfoot>
		        <tbody>
					@foreach($tobuys as $tobuy)
						<tr data-id={{ $tobuy->id }} >
						    <td>
							<span class="badge alert-tobuy">
						        {{$tobuy->priority}}
							</span>
						    </td>
						    <td><a href="{{ URL::action('TobuysController@priorityUp', [$tobuy->id] ) }}"><span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span></a></td>
						    <td><a href="{{ URL::action('TobuysController@priorityDown', [$tobuy->id] ) }}"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a></td>
							<td>
				    			{!!$tobuy->description!!}
							</td>	
							<td>
				    			{{$tobuy->cost}}
							</td>																	
							<td>
								<a href="{{ URL::action('TobuysController@edit', [$tobuy->id] ) }}">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								</a>
							</td>
							<td>
					    		{{date("j M", strtotime($tobuy->updated_at))}}
							</td>
							<td>
					    		<a href="{{ URL::action('TobuysController@done', [$tobuy->id] ) }}">
					    			@if(is_null($tobuy->completed_at))
					    				<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
					    			@else
					    				<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
					    			@endif	
					    		</a>
							</td>
							<td>
								<script>
			                          function ConfirmDelete()
			                          {
			                              var x = confirm("Are you sure you want to delete?");
			                              if (x)
			                                return true;
			                              else
			                                return false;
			                          }
			                    </script>
							    <a  onclick="return ConfirmDelete()" href="{{ URL::action('TobuysController@destroy', [$tobuy->id] ) }}"><span style="color:red" class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
	</div>


	<div id="donetable">
			<table width="100%" class="display" id="dones" cellspacing="0">
		        <thead>
		            <tr>            	
		                <th>Priority</th>
		                <th>Up</th>
		                <th>Down</th>
				        <th>Description</th>
				        <th>Cost &pound;</th>
		                <th>Edit</th>
		                <th>Updated</th>				                
		                <th>Bought</th> 
		                <th>Delete</th>   
		            </tr>
		        </thead>
		        <tfoot>
		            <tr>            	
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		                <th></th>
		            </tr>
		        </tfoot>
		        <tbody>
					@foreach($dones as $done)
						<tr data-id={{ $done->id }} >
						    <td>
							<span class="badge alert-done">
						        {{$done->priority}}
							</span>
						    </td>
						    <td><a href="{{ URL::action('TobuysController@priorityUp', [$done->id] ) }}"><span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span></a></td>
						    <td><a href="{{ URL::action('TobuysController@priorityDown', [$done->id] ) }}"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a></td>
							<td>
				    			{!!$done->description!!}
							</td>	
							<td>
				    			{{$done->cost}}
							</td>																	
							<td>
								<a href="{{ URL::action('TobuysController@edit', [$done->id] ) }}">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								</a>
							</td>
							<td>
					    		{{date("j M", strtotime($done->updated_at))}}
							</td>
							<td>
					    		<a href="{{ URL::action('TobuysController@done', [$done->id] ) }}">
					    			@if(is_null($done->completed_at))
					    				<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
					    			@else
					    				<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
					    			@endif	
					    		</a>
							</td>
							<td>
								<script>
			                          function ConfirmDelete()
			                          {
			                              var x = confirm("Are you sure you want to delete?");
			                              if (x)
			                                return true;
			                              else
			                                return false;
			                          }
			                    </script>
							    <a  onclick="return ConfirmDelete()" href="{{ URL::action('TobuysController@destroy', [$done->id] ) }}"><span style="color:red" class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			<?php echo $dones->render(); ?>
	</div>
</div>
<div class="col-md-1">

</div>


	<script>
	    $(document).ready(function() {
	        $('#tobuys').dataTable({
	        	"order": [[ 0, "desc" ]],
	        	"iDisplayLength": 25
	        });

	    } );
	</script>

@stop
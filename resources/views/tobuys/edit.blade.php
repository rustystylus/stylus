@extends('layouts.back')
@section('title')
tobuys
@stop
@section('content')

	@if(Session::has('flash_message'))
	    <div class="alert alert-success">
	        {{ Session::get('flash_message') }}
	    </div>
	@endif
	
	@include('partials.alerts.errors')

<h1>Edit tobuy </h1>
{!! Form::model($tobuy, [
    'method' => 'PATCH',
    'route' => ['tobuys.update', $tobuy->id]
]) !!}


<div class="form-group">
    {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('cost', 'cost', ['class' => 'control-label']) !!}
    {!! Form::number('cost', 0, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('priority', 'Priority', ['class' => 'control-label']) !!}
    {!! Form::number('priority', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit('Update tobuy', ['class' => 'btn btn-primary']) !!}

{!! Form::close() !!}
 <a href="{{ route('tobuys.index') }}">Go back to all tobuys.</a>


@stop
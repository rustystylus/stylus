@extends('layouts.back')

@section('title')
@stop
@section('content')
	<h1>Statements</h1>

		@if(Session::has('flash_message'))
		    <div class="alert alert-success">
		        {{ Session::get('flash_message') }}
		    </div>
		@endif

		@include('partials.alerts.errors')

	{!! Form::open([
	    'route' => 'statements.store'
	]) !!}

	<div class="form-group">
	    {!! Form::label('statement', 'Statement', ['class' => 'control-label']) !!}
	    {!! Form::text('statement', null, ['class' => 'form-control']) !!}
	</div>

	{!! Form::submit('Create New statement', ['class' => 'btn btn-primary']) !!}

	{!! Form::close() !!}
@stop
@extends('layouts.back')
@section('title')
statements
@stop
@section('content')

	@if(Session::has('flash_message'))
	    <div class="alert alert-success">
	        {{ Session::get('flash_message') }}
	    </div>
	@endif
	
	@include('partials.alerts.errors')

<h1>Edit statement </h1>
{!! Form::model($statement, [
    'method' => 'PATCH',
    'route' => ['statements.update', $statement->id]
]) !!}

<div class="form-group">
    {!! Form::label('statement', 'Statement', ['class' => 'control-label']) !!}
    {!! Form::text('statement', null, ['class' => 'form-control']) !!}
</div>


{!! Form::submit('Update statement', ['class' => 'btn btn-primary']) !!}

{!! Form::close() !!}
 <a href="{{ route('statements.index') }}">Go back to all statements.</a>


@stop
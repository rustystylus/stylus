@extends('layouts.front')

@section('title')
Statements
@stop
@section('content')
  <script>
  $( document ).ready(function() {
    DisplayStatement();
});

  </script>

<div class="col-md-2">

</div>
<div class="col-md-8">
		<h1 id="mydiv1">Statements</h1>
        <?php $count=0; ?>
        <ul id="statements">
				@foreach($statements as $statement)
				    <li id="{{$count}}">{!! $statement->statement!!}
            </li>
            <?php $count++; ?>
				@endforeach
        </ul>
</div>
<div class="col-md-2">
    <button  onclick="return DisplayStatement()" ontype="button" class="btn btn-default">Show Random Statement</button>
</div>

<div id="dialog" title="Basic dialog">
  <p></p>
</div>

<script>
      function DisplayStatement()
      {
          var rnd = Math.floor(Math.random() * $('#statements li').length) + 1;
          var statementContent = document.getElementById(rnd);
          var dialogContent = document.getElementById('dialog');
          dialogContent.innerHTML = statementContent.innerHTML;

          $(function() {
              $( "#dialog" ).dialog({
                                      width: 400
              });
          });
      }
</script>

@stop
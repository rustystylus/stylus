@extends('layouts.back')

@section('title')
@stop
@section('content')

	@if(Session::has('flash_message'))
	    <div class="alert alert-success">
	        {{ Session::get('flash_message') }}
	    </div>
	@endif
<div class="col-md-2">
<h1>statements</h1>
</div>
<div class="col-md-8">

	<a href="{{ route('statements.create') }}" class="btn btn-primary">Add New statement</a>
	<table class="table table-striped table-bordered">
		<thead>
		    <tr>
		        <td>Statement</td>
		    </tr>
		</thead>
	@foreach($statements as $statement)
	    <tr>
		    <td>{{ $statement->statement }}</td>
    
		    <td>
		        <a href="{{ route('statements.show', $statement->id) }}" class="btn btn-info">View statement</a>
		        <a href="{{ route('statements.edit', $statement->id) }}" class="btn btn-primary">Edit statement</a>
		    </td>
	    </tr>
	@endforeach
	</table>
</div>
<div class="col-md-2">

</div>
@stop
<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
 
    <title>@yield('title')</title>

    <link href="favicon.ico" type="image/x-icon" rel="icon" />
    <link href='http://fonts.googleapis.com/css?family=Permanent+Marker' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src={{ URL::asset('https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js')}}></script>
    
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{ URL::asset('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css') }}"/>

    <link rel="stylesheet" href="{{ URL::asset('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap-theme.min.css') }}"/>

    <link rel="stylesheet" href="css/back.css" />
</head>
 
<body>

    	@include('includes.backnav')

    	@include('includes.backheader')

        <div class="container">
            @yield('content')
        </div>

        @include('includes.backfooter')

        <script type="text/javascript" src={{ URL::asset('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js')}}></script>


</body>
</html>
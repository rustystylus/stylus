@extends('layouts.front')
  
@section('title')
        	 {{ $title }}
@stop   
@include('includes.frontcarousel')
@section('content')  

<main>
<div class="row row-eq-height">
    <div class="col-md-2">

    </div>
    <div class="col-md-8">

    					{!! $content !!}

    </div>
    <div class="col-md-2">

    </div>
</div>
</main>
@stop
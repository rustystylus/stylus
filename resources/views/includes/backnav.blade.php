<div class="navbar  navbar-inverse navbar-fixed-top" role="navigation">
<div class="container-fluid">
        <div class="nav navbar-header">

              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
        </div>
       <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
                <li><a href="{{ url('/') }}"><span class="glyphicon glyphicon-home"></span> </a></li>

                <li class="dropdown">
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle">Common additions<b class="caret"></b></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('/cbts') }}">CBTs</a></li>                        
                        <li><a href="{{ url('/questions') }}">Questions</a></li>                      
                        <li><a href="{{ url('/statements') }}">Statements</a></li>
                        <li><a href="{{ url('/todos') }}">Todos</a></li>  
                        <li><a href="{{ url('/tobuys') }}">Tobuys</a></li>                   
                    </ul>
                </li> 
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle">UnCommon additions<b class="caret"></b></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('/todotypes') }}">TodoTypes</a></li>  
                        <li><a href="{{ url('/freqs') }}">Freqs</a></li>                                          
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle">Page Content<b class="caret"></b></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('/sides') }}">Sides</a></li>
                        <li><a href="{{ url('/pages') }}">Pages</a></li>
                    </ul>
                </li>
        </ul>
                <!-- Right Side Of Navbar -->
        <ul class="nav navbar-nav navbar-right">

            <li><a href="{{ url('/back') }}"><span class="glyphicon glyphicon-cog"></span></a></li>

            <!-- Authentication Links -->
            @if (Auth::guest())
                <li><a href="{{ url('/login') }}">Login</a></li>
                <li><a href="{{ url('/register') }}">Register</a></li>
            @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                    </ul>
                </li>
            @endif
        </ul>
        </div> 
</div> 
</div>
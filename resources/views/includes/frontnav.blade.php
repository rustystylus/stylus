   <div class="navbar  navbar-inverse navbar-fixed-top navbar-custom" role="navigation">
    <div class="container-fluid">
            <div class="nav navbar-header">
                  <a href="{{ URL::to('/') }}" class="navbar-brand">
                      <img src="images/logo.png" alt="logo">
                  </a>
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
            </div>
            <div class="navbar-collapse collapse">

                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Tables <b class="caret"></b></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="frontTodos">Todos</a></li>
                                <li><a href="frontTobuys">Tobuys</a></li>
                                <li><a href="frontCbts">CBTs</a></li>
                                <li><a href="frontQuestions">Questions</a></li>
                                <li><a href="frontStatements">Statements</a></li>
                            </ul>
                        </li>
                    </ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{ url('/back') }}"><span class="glyphicon glyphicon-cog"></span></a></li>

                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                                </ul>
                            </li>
                        @endif
                    </ul>
            </div>
  </div>
</div>

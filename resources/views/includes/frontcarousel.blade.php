<!--  Carousel - consult the Twitter Bootstrap docs at
      http://twitter.github.com/bootstrap/javascript.html#carousel -->
<section id="carousel">
<div id="this-carousel-id" class="carousel slide"><!-- class of slide for animation -->
  <div class="carousel-inner">
    
      <div class="item active">   
          <img src="images/stylus1.jpg" class="img-responsive displayed" alt="img1">
              <div class="carousel-caption text-xs-left">
                <h1>Stylus</h1>
                <p>Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              </div>
      </div>

    <div class="item">
      <img src="images/stylus2.jpg" class="img-responsive displayed" alt="img2">
    </div>
    <div class="item">
      <img src="images/stylus3.jpg" class="img-responsive displayed" alt="img3">
    </div>
    <div class="item">     
      <img src="images/stylus4.jpg" class="img-responsive displayed" alt="img4">
    </div> 

  </div><!-- /.carousel-inner -->
  <!--  Next and Previous controls below
        href values must reference the id for this carousel -->
    <a class="carousel-control left" href="#this-carousel-id" data-slide="prev">&lsaquo;</a>
    <a class="carousel-control right" href="#this-carousel-id" data-slide="next">&rsaquo;</a>

</div>
        <script>
          $(document).ready(function(){
            $('.carousel').carousel({interval: 12000, cycle: true});
          });
        </script>
</section>